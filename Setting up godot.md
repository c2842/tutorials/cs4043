# Installing Godot
For the purporses of this I am going to be using the C# version of godot.  
From what I can tell C# is a good lnguage to pick up, downside is we need to install a few extra things to get it up to speed.

We are also going to follow their own tutorial https://docs.godotengine.org/en/stable/tutorials/scripting/c_sharp/c_sharp_basics.html#doc-c-sharp-setup

## To download
* Godot Mono (https://godotengine.org/download/windows)
* DotNet (.Net) (https://dotnet.microsoft.com/en-us/download/dotnet)
  * Download both 6.0 and Core
  * Make sure you get the x64 versions
  * Make sure you download the installer and not the binaries.

Godot does not install, its a standalone binary

## Install Rider
If you are using the Intellij toolbox simply select it from teh menu.  
If not then start using teh toolbox

Plugins: Godot and GDScript (3rd party)
